import React, { Component } from 'react';
import './InfoMessage.css'
import { AbacusConsumer } from "../contexts/AppContext";


class InfoMessage extends Component {

    render() {
        return (
            <AbacusConsumer>
                {context => {
                    if (context.state.inFullAbacus) {
                        return <p className="value">{context.state.value}</p>
                    } else {
                        if (context.state.level === "counting") {
                            return <p className="value">{context.state.value}</p>
                        } else if (context.state.sublevel === "visualization") {
                            if (context.state.tutorialState.sequence[context.state.tutorialState.currentExercise].answer === context.state.value) {
                                return <div className="text">Сброс и<br /> далее</div>
                            } else {
                                return <p className="value">{context.state.value}</p>
                            }

                        } else {
                            if (context.state.tutorialState.sequence[context.state.tutorialState.currentExercise].seq.length === context.state.ansPosition &&
                                context.state.tutorialState.sequence[context.state.tutorialState.currentExercise].answer === context.state.value
                            ) {
                                return <div className="text">Сброс и<br /> далее</div>
                            } else {
                                return <p className="value">{context.state.value}</p>
                            }
                        }
                    }



                }}
            </AbacusConsumer>
        )
    }

}

export default InfoMessage
