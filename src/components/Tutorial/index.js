import React from "react";
import { AbacusContext } from "../../contexts/AppContext";
import Tutorial from "./Tutorial";

export default props => (
    <AbacusContext.Consumer>
        {
            ({ state: {tutorialState, visibility, gameCompeted, level, sublevel, gameState}, actions: {restartTutorial, showMenu} }) => <Tutorial 
        {...props} 

        tutorialState={tutorialState}
        restartTutorial={restartTutorial} 
        visibility={visibility} 
        gameCompeted={gameCompeted}
        showMenu={showMenu}
        level={level}
        sublevel={sublevel}
        gameState={gameState}
        />}
    </AbacusContext.Consumer>
);

