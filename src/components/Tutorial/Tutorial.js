import React from 'react';

import Abacus from '../Abacus'
import Exercise from '../Exercise'
import Status from '../Status'
import Hint from '../Hint'
import Helper from '../Helper/Helper'
import "./Tutorial.css"
import mouse from './mouse.svg'


class Tutorial extends React.Component {

  componentDidMount() {
    document.body.style = 'background: #EDC0D7;';
  }

  render() {
    let tratata;
    try {
      tratata = this.props.tutorialState.sequence[this.props.tutorialState.currentExercise].vmode;
    } catch (e){
      tratata = this.props.tutorialState.sequence[this.props.tutorialState.currentExercise - 1].vmode;
    }


    return (
      <>
        <div className="overlay" style={this.props.gameState[this.props.level].sublevels[this.props.sublevel].status === 2 ? { display: 'block' } : { display: 'none' }}>
          <div className="popupbox" style={{ backgroundImage: "url(" + mouse + ")" }}>
            <div className="titlep">Молодец!</div>
            <div className="textp"><span>Ты успешно прошел все упражнения в этом уроке!</span></div>
            <div className="buttonp" onClick={(e) => this.props.showMenu()}><span>Меню</span></div>
            <div className="restartp" onClick={(e) => this.props.restartTutorial()}><span>Заново</span></div>

          </div>
        </div>

        <div className="app" id="game">
          <div className="helper">
            <div className="back" onClick={(e) => this.props.showMenu()}></div>
            <div className="progress"><Status /></div>
            <div className="resetgame" onClick={(e) => this.props.restartTutorial()}>

            </div>
            <div className="helpertext">
              <Helper level={this.props.level} sublevel={this.props.sublevel} mode={tratata} />
            </div>



          </div>
          <div className="exercise">
            <Hint />
            <div className="eyes"></div>
            <Exercise />
          </div>
          <div className="abacusholder"
            id="abacusholder" >
            <Abacus />
          </div>

        </div>
      </>
    )
  }
}


export default Tutorial

