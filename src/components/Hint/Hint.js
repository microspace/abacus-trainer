import React from 'react';
import './Hint.css'


/* const hints = {
  "simple": "Просто + и -",
  "simpleviz": "Визуализация",
  "brother+1": "Помощь брата +",
  "brother+2": "Помощь брата +",
  "brother+3": "Помощь брата +",
  "brother+4": "Помощь брата +",
  "brother+1234": "Помощь брата +",
  "brother-1": "Помощь брата -",
  "brother-2": "Помощь брата -",
  "brother-3": "Помощь брата -",
  "brother-4": "Помощь брата -",
  "brother-1234": "Помощь брата -",

} */
const hints = {
  "counting": "Счет на абакусе",
  "plusminus": "Сложение/вычитание",
  "brother+": "Помощь брата",
  "brother-": "Помощь брата",
  "friend+": "Помощь друга",
  "friend-": "Помощь друга",
  "combo+": "Комбинированный",
  "combo-": "Комбинированный",
  "free": "Произвольный",
}
class Hint extends React.Component {
  render() {
    return (
      <div className="hints">{hints[this.props.level]}</div>
    )
  }
}


export default Hint
