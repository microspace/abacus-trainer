import React from "react";
import { AbacusContext } from "../../contexts/AppContext";
import Menu from "./Menu";

export default props => (
    <AbacusContext.Consumer>
        {
            ({ state: {gameState, level, sublevel }, actions: {changeLevel, startGame, showFullAbacus} }) => <Menu 
        {...props} 
        gameState={gameState}
        startGame={startGame}
        level={level}
        sublevel={sublevel}
        changeLevel={changeLevel}
        showFullAbacus={showFullAbacus}

        />}
    </AbacusContext.Consumer>
);

