import React, { Component } from 'react';
import './Menu.css';
import Icon1234 from "./icons/icon1234.svg"
import Icon_signs from "./icons/icon_signs.svg"
import Icon_5p from "./icons/icon_5p.svg"
import Icon_5m from "./icons/icon_5m.svg"
import Icon_10p from "./icons/icon_10p.svg"
import Icon_10m from "./icons/icon_10m.svg"
import Icon_p from "./icons/icon_p.svg"
import Icon_m from "./icons/icon_m.svg"
import Icon_rnd from "./icons/icon_rnd.svg"
import Background from "./icons/submenufon.svg"
import AbacusIcon from "./icons/abacusicon.svg"
import { TiArrowShuffle } from "react-icons/ti";
import { FaGraduationCap } from "react-icons/fa";

const menulayout = [
    {
        name: "counting",
        localname: "Счёт на абакусе",
        icon: Icon1234,
        subelevels: [
            {
                name: "tutorial",
                localname: "Обучение",
                status: 0,
                icon: <FaGraduationCap style={{ fontSize: "1.5em" }} />
            },
            {
                name: "forward",
                localname: "Счёт",
                status: 0,
                icon: "1 2 3"
            },
            {
                name: "reverse",
                localname: "Счёт назад",
                status: 0,
                icon: "3 2 1"
            },
            {
                name: "random",
                localname: "Случайные",
                status: 0,
                icon: <TiArrowShuffle style={{ fontSize: "1.5em" }} />
            }
        ]
    },
    {
        name: "plusminus",
        localname: "Сложение/вычитание",
        icon: Icon_signs,

        subelevels: [
            {
                name: "tutorial",
                localname: "Обучение",
                status: 0,
                icon: <FaGraduationCap style={{ fontSize: "1.5em" }} />
            },
            {
                name: "pm1234",
                localname: "±1234",
                status: 0,
                icon: "±1234"
            },
            {
                name: "pm5",
                localname: "±5",
                status: 0,
                icon: "±5"
            },
            {
                name: "pm6",
                localname: "±6",
                status: 0,
                icon: "±6"
            },
            {
                name: "pm7",
                localname: "±7",
                status: 0,
                icon: "±7"
            },
            {
                name: "pm8",
                localname: "±8",
                status: 0,
                icon: "±8"
            },
            {
                name: "pm9",
                localname: "±9",
                status: 0,
                icon: "±9"
            },
            {
                name: "random",
                localname: "Случайные",
                status: 0,
                icon: <TiArrowShuffle style={{ fontSize: "1.5em" }} />
            },
            {
                name: "visualization",
                localname: "Счёт в уме",
                status: 0,
                icon: <svg width="18" height="35" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clipPath="url(#clip0)">
                        <path d="M9 1.09088C1.36046 1.09088 0.941857 5.09088 1.46511 9.99997C1.98837 14.9091 5.54651 18.9091 9 18.9091C12.4535 18.9091 16.0116 14.9091 16.5349 9.99997C17.0581 5.18179 16.6395 1.09088 9 1.09088Z" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinejoin="round" />
                        <path d="M16.4302 8.72722C13.186 7.99994 11.1977 6.18176 11.1977 6.18176C11.1977 6.18176 10.3605 7.81813 6.27907 8.36358" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="18" height="20" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
            },
        ]
    },
    {
        name: "brother+",
        localname: "Помощь брата",
        icon: Icon_5p,

        subelevels: [
            {
                name: "tutorial",
                localname: "Обучение",
                status: 0,
                icon: <FaGraduationCap style={{ fontSize: "1.5em" }} />
            },
            {
                name: "brother+1",
                localname: "+1",
                status: 0,
                icon: "+1"
            },
            {
                name: "brother+2",
                localname: "+2",
                status: 0,
                icon: "+2"
            },
            {
                name: "brother+3",
                localname: "+3",
                status: 0,
                icon: "+3"
            },
            {
                name: "brother+4",
                localname: "+4",
                status: 0,
                icon: "+4"
            },
            {
                name: "random",
                localname: "Случайные",
                status: 0,
                icon: <TiArrowShuffle style={{ fontSize: "1.5em" }} />
            },
            {
                name: "visualization",
                localname: "Счёт в уме",
                status: 0,
                icon: <svg width="18" height="35" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clipPath="url(#clip0)">
                        <path d="M9 1.09088C1.36046 1.09088 0.941857 5.09088 1.46511 9.99997C1.98837 14.9091 5.54651 18.9091 9 18.9091C12.4535 18.9091 16.0116 14.9091 16.5349 9.99997C17.0581 5.18179 16.6395 1.09088 9 1.09088Z" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinejoin="round" />
                        <path d="M16.4302 8.72722C13.186 7.99994 11.1977 6.18176 11.1977 6.18176C11.1977 6.18176 10.3605 7.81813 6.27907 8.36358" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="18" height="20" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
            },
        ]
    },
    {
        name: "brother-",
        localname: "Помощь брата",
        icon: Icon_5m,

        subelevels: [
            {
                name: "tutorial",
                localname: "Обучение",
                status: 0,
                icon: <FaGraduationCap style={{ fontSize: "1.5em" }} />
            },
            {
                name: "brother-1",
                localname: "-1",
                status: 0,
                icon: "-1"
            },
            {
                name: "brother-2",
                localname: "-2",
                status: 0,
                icon: "-2"
            },
            {
                name: "brother-3",
                localname: "-3",
                status: 0,
                icon: "-3"
            },
            {
                name: "brother-4",
                localname: "-4",
                status: 0,
                icon: "-4"
            },
            {
                name: "random",
                localname: "Случайные",
                status: 0,
                icon: <TiArrowShuffle style={{ fontSize: "1.5em" }} />
            },
            {
                name: "visualization",
                localname: "Счёт в уме",
                status: 0,
                icon: <svg width="18" height="35" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clipPath="url(#clip0)">
                        <path d="M9 1.09088C1.36046 1.09088 0.941857 5.09088 1.46511 9.99997C1.98837 14.9091 5.54651 18.9091 9 18.9091C12.4535 18.9091 16.0116 14.9091 16.5349 9.99997C17.0581 5.18179 16.6395 1.09088 9 1.09088Z" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinejoin="round" />
                        <path d="M16.4302 8.72722C13.186 7.99994 11.1977 6.18176 11.1977 6.18176C11.1977 6.18176 10.3605 7.81813 6.27907 8.36358" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="18" height="20" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
            },
        ]
    },
    {
        name: "friend+",
        localname: "Помощь друга",
        icon: Icon_10p,

        subelevels: [
            {
                name: "tutorial",
                localname: "Обучение",
                status: 0,
                icon: <FaGraduationCap style={{ fontSize: "1.5em" }} />
            },
            {
                name: "friend+9",
                localname: "+9",
                status: 0,
                icon: "+9"
            },
            {
                name: "friend+8",
                localname: "+8",
                status: 0,
                icon: "+8"
            },
            {
                name: "friend+7",
                localname: "+7",
                status: 0,
                icon: "+7"
            },
            {
                name: "friend+6",
                localname: "+6",
                status: 0,
                icon: "+6"
            },
            {
                name: "friend+5",
                localname: "+5",
                status: 0,
                icon: "+5"
            },
            {
                name: "friend+4",
                localname: "+4",
                status: 0,
                icon: "+4"
            },
            {
                name: "friend+3",
                localname: "+3",
                status: 0,
                icon: "+3"
            },
            {
                name: "friend+2",
                localname: "+2",
                status: 0,
                icon: "+2"
            },
            {
                name: "friend+1",
                localname: "+1",
                status: 0,
                icon: "+1"
            },
            {
                name: "random",
                localname: "Случайные",
                status: 0,
                icon: <TiArrowShuffle style={{ fontSize: "1.5em" }} />
            },
            {
                name: "visualization",
                localname: "Счёт в уме",
                status: 0,
                icon: <svg width="18" height="35" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clipPath="url(#clip0)">
                        <path d="M9 1.09088C1.36046 1.09088 0.941857 5.09088 1.46511 9.99997C1.98837 14.9091 5.54651 18.9091 9 18.9091C12.4535 18.9091 16.0116 14.9091 16.5349 9.99997C17.0581 5.18179 16.6395 1.09088 9 1.09088Z" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinejoin="round" />
                        <path d="M16.4302 8.72722C13.186 7.99994 11.1977 6.18176 11.1977 6.18176C11.1977 6.18176 10.3605 7.81813 6.27907 8.36358" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="18" height="20" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
            },
        ]
    },
    {
        name: "friend-",
        localname: "Помощь друга",
        icon: Icon_10m,

        subelevels: [
            {
                name: "tutorial",
                localname: "Обучение",
                status: 0,
                icon: <FaGraduationCap style={{ fontSize: "1.5em" }} />
            },
            {
                name: "friend-9",
                localname: "-9",
                status: 0,
                icon: "-9"
            },
            {
                name: "friend-8",
                localname: "-8",
                status: 0,
                icon: "-8"
            },
            {
                name: "friend-7",
                localname: "-7",
                status: 0,
                icon: "-7"
            },
            {
                name: "friend-6",
                localname: "-6",
                status: 0,
                icon: "-6"
            },
            {
                name: "friend-5",
                localname: "-5",
                status: 0,
                icon: "-5"
            },
            {
                name: "friend-4",
                localname: "-4",
                status: 0,
                icon: "-4"
            },
            {
                name: "friend-3",
                localname: "-3",
                status: 0,
                icon: "-3"
            },
            {
                name: "friend-2",
                localname: "-2",
                status: 0,
                icon: "-2"
            },
            {
                name: "friend-1",
                localname: "-1",
                status: 0,
                icon: "-1"
            },
            {
                name: "random",
                localname: "Случайные",
                status: 0,
                icon: <TiArrowShuffle style={{ fontSize: "1.5em" }} />
            },
            {
                name: "visualization",
                localname: "Счёт в уме",
                status: 0,
                icon: <svg width="18" height="35" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clipPath="url(#clip0)">
                        <path d="M9 1.09088C1.36046 1.09088 0.941857 5.09088 1.46511 9.99997C1.98837 14.9091 5.54651 18.9091 9 18.9091C12.4535 18.9091 16.0116 14.9091 16.5349 9.99997C17.0581 5.18179 16.6395 1.09088 9 1.09088Z" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinejoin="round" />
                        <path d="M16.4302 8.72722C13.186 7.99994 11.1977 6.18176 11.1977 6.18176C11.1977 6.18176 10.3605 7.81813 6.27907 8.36358" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="18" height="20" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
            },
        ]
    },
    {
        name: "combo+",
        localname: "Комбинированный",
        icon: Icon_p,

        subelevels: [
            {
                name: "tutorial",
                localname: "Обучение",
                status: 0,
                icon: <FaGraduationCap style={{ fontSize: "1.5em" }} />
            },
            {
                name: "combo+9",
                localname: "+9",
                status: 0,
                icon: "+9"
            },
            {
                name: "combo+8",
                localname: "+8",
                status: 0,
                icon: "+8"
            },
            {
                name: "combo+7",
                localname: "+7",
                status: 0,
                icon: "+7"
            },
            {
                name: "combo+6",
                localname: "+6",
                status: 0,
                icon: "+6"
            },
            {
                name: "random",
                localname: "Случайные",
                status: 0,
                icon: <TiArrowShuffle style={{ fontSize: "1.5em" }} />
            },
            {
                name: "visualization",
                localname: "Счёт в уме",
                status: 0,
                icon: <svg width="18" height="35" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clipPath="url(#clip0)">
                        <path d="M9 1.09088C1.36046 1.09088 0.941857 5.09088 1.46511 9.99997C1.98837 14.9091 5.54651 18.9091 9 18.9091C12.4535 18.9091 16.0116 14.9091 16.5349 9.99997C17.0581 5.18179 16.6395 1.09088 9 1.09088Z" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinejoin="round" />
                        <path d="M16.4302 8.72722C13.186 7.99994 11.1977 6.18176 11.1977 6.18176C11.1977 6.18176 10.3605 7.81813 6.27907 8.36358" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="18" height="20" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
            },
        ]
    },
    {
        name: "combo-",
        localname: "Комбинированный",
        icon: Icon_m,

        subelevels: [
            {
                name: "tutorial",
                localname: "Обучение",
                status: 0,
                icon: <FaGraduationCap style={{ fontSize: "1.5em" }} />
            },
            {
                name: "combo-9",
                localname: "-9",
                status: 0,
                icon: "-9"
            },
            {
                name: "combo-8",
                localname: "-8",
                status: 0,
                icon: "-8"
            },
            {
                name: "combo-7",
                localname: "-7",
                status: 0,
                icon: "-7"
            },
            {
                name: "combo-6",
                localname: "-6",
                status: 0,
                icon: "-6"
            },
            {
                name: "random",
                localname: "Случайные",
                status: 0,
                icon: <TiArrowShuffle style={{ fontSize: "1.5em" }} />
            },
            {
                name: "visualization",
                localname: "Счёт в уме",
                status: 0,
                icon: <svg width="18" height="35" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clipPath="url(#clip0)">
                        <path d="M9 1.09088C1.36046 1.09088 0.941857 5.09088 1.46511 9.99997C1.98837 14.9091 5.54651 18.9091 9 18.9091C12.4535 18.9091 16.0116 14.9091 16.5349 9.99997C17.0581 5.18179 16.6395 1.09088 9 1.09088Z" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinejoin="round" />
                        <path d="M16.4302 8.72722C13.186 7.99994 11.1977 6.18176 11.1977 6.18176C11.1977 6.18176 10.3605 7.81813 6.27907 8.36358" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="18" height="20" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
            },
        ]
    },
    {
        name: "free",
        localname: "Произвольный",
        icon: Icon_rnd,

        subelevels: [
            {
                name: "tutorial",
                localname: "Обучение",
                status: 0,
                icon: <FaGraduationCap style={{ fontSize: "1.5em" }} />
            },
            {
                name: "random",
                localname: "Случайные",
                status: 0,
                icon: <TiArrowShuffle style={{ fontSize: "1.5em" }} />
            },
            {
                name: "visualization",
                localname: "Счёт в уме",
                status: 0,
                icon: <svg width="18" height="35" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clipPath="url(#clip0)">
                        <path d="M9 1.09088C1.36046 1.09088 0.941857 5.09088 1.46511 9.99997C1.98837 14.9091 5.54651 18.9091 9 18.9091C12.4535 18.9091 16.0116 14.9091 16.5349 9.99997C17.0581 5.18179 16.6395 1.09088 9 1.09088Z" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinejoin="round" />
                        <path d="M16.4302 8.72722C13.186 7.99994 11.1977 6.18176 11.1977 6.18176C11.1977 6.18176 10.3605 7.81813 6.27907 8.36358" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                    </g>
                    <defs>
                        <clipPath id="clip0">
                            <rect width="18" height="20" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
            },
        ]
    },
]


const objToList = [
    "counting",
    "plusminus",
    "brother+",
    "brother-",
    "friend+",
    "friend-",
    "combo+",
    "combo-",
    "free",
]
class Menu extends Component {

    render() {
        return (
            <div className="menulayout">
                <div className="header">
                    <div className="backm" onClick={(e) => window.location.href = "#close"}></div>
                    <div className="titlem">Выбери интересующий тебя раздел или начни тренировки с первого упражнения. </div>
                    <div
                        className="fullabacus"
                        onClick={() => this.props.showFullAbacus()}
                        style={{ backgroundImage: `url(${AbacusIcon})` }}
                    >
                    </div>
                </div>
                <div className="menulist">
                    {menulayout.map((menuitem, i) => {
                        return (
                            <div className={this.props.level === menuitem.name ? "menuitem mactive" : "menuitem"}
                                key={menuitem.name}
                                onClick={() => this.props.changeLevel(menuitem.name)}
                            >
                                <p className="itemtext"><img className="menuicon" src={menuitem.icon} alt="menuicon" />{menuitem.localname}</p><span className="angle">›</span>
                            </div>
                        )
                    }
                    )}
                </div>
                <div className="submenu">
                    {menulayout[objToList.indexOf(this.props.level)].subelevels.map((sublevelitem) => {
                        return (
                            <div className="submenuitem"
                                key={sublevelitem.name}
                                style={{ backgroundImage: `url(${Background})` }}
                                onClick={() => this.props.startGame(this.props.level, sublevelitem.name)}
                            >
                                <div className="upperpart">
                                    <div className="submenuicon">
                                        {sublevelitem.icon}
                                    </div>
                                    <SublevelStatus
                                        currentExercise={this.props.gameState[this.props.level]}
                                        sublevel={sublevelitem.name}
                                    />
                                </div>
                                <div className="loverpart">
                                    <p className="submenutext">{sublevelitem.localname}</p><span className="angle_small">›</span>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default Menu



const SublevelStatus = (props) => {


    if (!props.currentExercise.sublevels.hasOwnProperty(props.sublevel)) {
        return (
            <div className="gamestatus">
            </div>
        )
    } else {

        if (props.currentExercise.sublevels[props.sublevel].status === 2) {
            return (
                <div className="gamestatus completed">
                </div>
            )

        } else if (props.currentExercise.sublevels[props.sublevel].status === 1) {
            return (
                <div className="gamestatus started">
                </div>
            )
        } else {
            return (
                <div className="gamestatus">
                </div>
            )
        }

    }

}