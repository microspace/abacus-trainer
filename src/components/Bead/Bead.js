import React, { Component } from 'react';
import './Bead.css'
import { AbacusConsumer } from "../../contexts/AppContext";

class Bead extends Component {

    render() {

        let tclass;
        if (this.props.row >= 1) {
            if (this.props.column === 0 || this.props.column === 8) {
                tclass = `bead${this.props.row} bead_bottom`
            } else {
                tclass = `bead${this.props.row} bead_bottom bead_leftd`
            }

        } else if (this.props.row === 0) {
            if (this.props.column === 0) {
                tclass = "bead5 bead_upper"
            }
            else {
                tclass = `bead5 bead_upper bead_leftu`
            }
        }

        return (
            <AbacusConsumer>
                {context => {
                    let y = context.state.abacusState[this.props.column][this.props.row].currentY;

                    let styles = {
                        transform: `translateY(${y}px)`
                    };

                    return (
                        <div
                            style={styles}
                            className={`${tclass}`}
                            id={this.props.column + " " + this.props.row}

                        /*                          onTouchStart={(e) => context.actions.dragStart(e, this.props.column, this.props.row)}
                                                    onTouchMove={(e) => context.actions.drag(e, this.props.column, this.props.row)}
                                                    onTouchEnd={(e) => context.actions.dragEnd(e, this.props.column, this.props.row)}
                        
                                                    onMouseDown={(e) => context.actions.dragStart(e, this.props.column, this.props.row)}
                                                    onMouseMove={(e) => context.actions.drag(e, this.props.column, this.props.row)}
                                                    onMouseUp={(e) => context.actions.dragEnd(e, this.props.column, this.props.row)} */
                        ></div>
                    )
                }}
            </AbacusConsumer>
        );
    }
}


export default Bead