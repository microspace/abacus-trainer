import React from "react";
import { AbacusContext } from "../../contexts/AppContext";
import TitleScreen from "./TitleScreen";

export default props => (
    <AbacusContext.Consumer>
        {
            ({ state: {tutorialState}, actions: {showMenu } }) => <TitleScreen 
        {...props} 
        tutorialState={tutorialState}
        showMenu={showMenu}

        />}
    </AbacusContext.Consumer>
);

