import React, { Component } from 'react';
import './TitleScreen.css';

class TitleScreen extends Component {
    render() {
        return (
            <div className="titleback">
                <div className="title">абакус</div>
                <div className="subtitle">туториал</div>
                <div className="start" onClick={(e) => this.props.showMenu()}>Начать</div>

            </div>
        )
    }
}

export default TitleScreen