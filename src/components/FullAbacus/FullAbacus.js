import React from 'react';

import Abacus from '../Abacus'

import "./FullAbacus.css"

class FullAbacus extends React.Component {
    componentDidMount() {
        document.body.style = 'background: #EDC0D7;';
    }
    render() {
        return <Abacus />
    }
}

export default FullAbacus

