import React from 'react';


class Timer extends React.Component {

  componentWillUnmount() {
    this.props.stopTimer();
  }

  componentDidMount() {
     if (this.props.allLevelsState[this.props.selectedOperation].sublevels[this.props.selectedSublevel].gameState !== "completed") {
      this.props.startTimer(this.props.selectedOperation, this.props.selectedSublevel);
    } 
  }

  render() {
    
/*     this.props.allLevelsState[this.props.selectedOperation].sublevels[this.props.selectedSublevel].elapsedTime = this.state.time;

    let gameState = this.props.allLevelsState[this.props.selectedOperation].sublevels[this.props.selectedSublevel].gameState

    let stop = (this.state.time === 0 || !this.state.isOn) ?
      null :
      <button onClick={this.stopTimer}>ПАУЗА</button>
    let resume = (this.state.time === 0 || this.state.isOn || gameState === "completed") ?
      null :
      <button onClick={this.startTimer}>ПРОДОЛЖИТЬ</button>

     */
    
    let stats = (this.props.allLevelsState[this.props.selectedOperation].sublevels[this.props.selectedSublevel].gameState === "completed") ?
      null : (<p>
      {this.props.allLevelsState[this.props.selectedOperation].sublevels[this.props.selectedSublevel].currentExercise}
      /
    {this.props.allLevelsState[this.props.selectedOperation].sublevels[this.props.selectedSublevel].exerciseCount}
    </p>)

    return (
      <div>
        <h3>{toHHMMSS(this.props.allLevelsState[this.props.selectedOperation].sublevels[this.props.selectedSublevel].elapsedTime)}</h3>
        <div>{stats}</div>
{/*         
        {resume}
        {stop}
        <br />
         */}
      </div>
    )
  }
}


export default Timer

const toHHMMSS = (time) => {
  var sec_num = parseInt(time, 10); // don't forget the second param
  var hours = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);

  if (hours < 10) { hours = "0" + hours; }
  if (minutes < 10) { minutes = "0" + minutes; }
  if (seconds < 10) { seconds = "0" + seconds; }
  return minutes + ':' + seconds;
}