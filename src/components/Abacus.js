import React, { Component } from 'react';
import './Abacus.css';
import Bead from './Bead/Bead';
import InfoMessage from './InfoMessage'
import { AbacusConsumer, AbacusContext } from "../contexts/AppContext";
import { Motion, spring } from 'react-motion';


class AbacusTest extends Component {
    static contextType = AbacusContext;
    resize = () => this.forceUpdate()

    componentDidMount() {
        window.addEventListener('resize', this.resize);
        this.forceUpdate()
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.resize)
    }
    forceUpdate() {

        if (this.context.state.abacusState.length <= 3) {
            let ar = 334 / 223;

            let abacus = document.getElementById("abacus");
            let abacusholder = document.getElementById("abacusholder");

            if (abacusholder.clientHeight > abacusholder.clientWidth * ar) {
                abacus.style.width = '100%';
                abacus.style.height = abacus.clientWidth * ar + "px";
            } else {
                abacus.style.height = '100%';
                abacus.style.width = abacus.clientHeight / ar + "px";
            }
        } else {
            let ar = 323 / 629;
            let abacus = document.getElementById("abacus");


            if (window.innerHeight > window.innerWidth * ar) {
                abacus.style.width = '100%';
                abacus.style.height = abacus.clientWidth * ar + "px";
            } else {
                abacus.style.height = '100%';
                abacus.style.width = abacus.clientHeight / ar + "px";
            }
        }

    }

    render() {
        return (
            <AbacusConsumer>
                {context => {
                    return (
                        <Motion style={{ opacity: spring(context.state.visibility ? 1 : 0) }}>
                            {({ opacity }) =>
                                <>
                                    <div style={{ opacity: 1 - opacity }} className="taptoanswer"><p>Коснитесь для ответа</p></div>
                                    <div
                                        style={{ opacity: opacity }}
                                        className={"abacus" + context.state.abacusState.length}
                                        id="abacus"
                                        onTouchStart={(e) => context.actions.dragStart(e)}
                                        onTouchMove={(e) => context.actions.drag(e)}
                                        onTouchEnd={(e) => context.actions.dragEnd(e)}
                                        onMouseDown={(e) => context.actions.dragStart(e)}
                                        onMouseMove={(e) => context.actions.drag(e)}
                                        onMouseUp={(e) => context.actions.dragEnd(e)}
                                    >
                                        <div className="backina" onClick={(e) => context.actions.showMenu()}></div>
                                        <div className="blankarea"></div>
                                        {context.state.abacusState.length === 9 ? <div className="leftedge edgefillvert"></div> : null}

                                        <div className="button_holder" id="reset"
                                            onMouseDown={(e) => context.actions.resetStart(e)}
                                            onMouseUp={(e) => context.actions.resetEnd(e)}
                                            onTouchStart={(e) => context.actions.resetStart(e)}
                                            onTouchEnd={(e) => context.actions.resetEnd(e)}
                                        >
                                            <InfoMessage />
                                        </div>
                                        <div className="upperedge edgefillhor"></div>
                                        {context.state.abacusState.map((column, i) => (
                                            <React.Fragment key={"column_" + i}>
                                                <div className={"uppercontainer" + i} id={"uc" + i}>
                                                    <Bead
                                                        column={i}
                                                        row={0}
                                                    />
                                                </div>
                                                <div className={"separator" + i + " sepfill"} id={"sp" + i}>
                                                    {
                                                        i === 0 ? <div className="circle" id="circle"></div> : null
                                                    }
                                                </div>
                                                <div className={"bottomcontainer" + i} id={"bc" + i}>
                                                    <div className="beadspacer"></div>
                                                    <Bead
                                                        column={i}
                                                        row={1}
                                                    />
                                                    <Bead
                                                        column={i}
                                                        row={2}
                                                    />
                                                    <Bead
                                                        column={i}
                                                        row={3}
                                                    />
                                                    <Bead
                                                        column={i}
                                                        row={4}
                                                    />
                                                </div>
                                            </React.Fragment>
                                        ))}
                                        <div className="bottomedge edgefillhor">
                                        </div>
                                        <div className="rightedge edgefillvert">
                                        </div>
                                    </div>
                                </>
                            }
                        </Motion>
                    )
                }}
            </AbacusConsumer>
        )
    }
}

export default AbacusTest


