import React, { Component, useContext } from 'react';
import './App.css';


import Tutorial from './components/Tutorial'
import TitleScreen from './components/TitleScreen'
import FullAbacus from './components/FullAbacus/FullAbacus'
import Menu from './components/Menu'
import { AbacusProvider, AbacusContext } from "./contexts/AppContext";

import { css } from '@emotion/core';
// First way to import
import { ClipLoader } from 'react-spinners';
// Another way to import


const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

function preventPullToRefresh(element) {
    var prevent = false;
console.log(document.querySelector(element))
    document.querySelector(element).addEventListener('touchstart', function(e){
      if (e.touches.length !== 1) { return; }

      var scrollY = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop;
      prevent = (scrollY === 0);
    });

    document.querySelector(element).addEventListener('touchmove', function(e){
      if (prevent) {
        prevent = false;
        e.preventDefault();
      }
    });
  }

class App extends Component {
    componentDidMount () {
          preventPullToRefresh('#root') // pass #id or html tag into the method
    }


    render() {
        return (
            <AbacusProvider location={this.props.location}>
                <Layout />
            </AbacusProvider>
        )
    }
}

export default App



function Layout() {

    let context = useContext(AbacusContext)

    if (context.state.loading) {
        return (
            <div className='sweet-loading'>
                <ClipLoader
                    css={override}
                    sizeUnit={"px"}
                    size={150}
                    color={'#123abc'}
                    loading={context.state.loading}
                />
            </div>
        )
    } else if (context.state.inTitle) {
        return <TitleScreen />
    } else if (context.state.inMenu) {
        return <Menu />
    } else if (context.state.inFullAbacus) {
        return <FullAbacus />
    } else {
        return <Tutorial />
    }



}


