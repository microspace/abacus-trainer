import React, { Component } from 'react';
import axios from 'axios';
import queryString from 'query-string';
import WebFont from 'webfontloader';
import { generateEmptyAbacus, calculateValue, resizeAbacus, generateSecuence } from './functions/cleanfunctions'
WebFont.load({
    google: {
        families: ['Rubik:400,500,700,900', 'sans-serif']
    }
});
export const AbacusContext = React.createContext();
export class AbacusProvider extends Component {
    startResetDrag = false;
    resetX0 = 0;
    resetx1 = 0;
    resetDx = 0;
    state = {
        inMenu: false,
        inTitle: true,
        inFullAbacus: false,
        visibility: true,
        loading: true,
        ansPosition: 0,
        elapsedSec: 0,
        showWinPopup: false,
        level: "counting",
        sublevel: null,
        gameCompeted: false,
        gameStarted: false,
        seq: [],
        ans: null,
        beadHeight: 0,
        value: 0,
        dragged: {
            c: null,
            r: null
        },
        active: false,
        enableAbacus: true,
        allLevelsState: [],
        abacusState: [],
        gameState: {
            "counting": {
                sublevels: {
                    "forward": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "reverse": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "random": {
                        currentExercise: 0,
                        status: 0,
                    }
                }
            },
            "plusminus": {
                sublevels: {
                    "pm1234": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "pm5": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "pm6": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "pm7": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "pm8": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "pm9": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "random": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "visualization": {
                        currentExercise: 0,
                        status: 0,
                    },
                }
            },
            "brother+": {
                sublevels: {
                    "brother+1": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "brother+2": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "brother+3": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "brother+4": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "random": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "visualization": {
                        currentExercise: 0,
                        status: 0,
                    },
                }
            },
            "brother-": {
                sublevels: {
                    "brother-1": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "brother-2": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "brother-3": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "brother-4": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "random": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "visualization": {
                        currentExercise: 0,
                        status: 0,
                    },
                }
            },
            "friend+": {
                sublevels: {
                    "friend+1": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend+2": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend+3": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend+4": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend+5": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend+6": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend+7": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend+8": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend+9": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "random": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "visualization": {
                        currentExercise: 0,
                        status: 0,
                    },
                }
            },
            "friend-": {
                sublevels: {
                    "friend-1": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend-2": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend-3": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend-4": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend-5": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend-6": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend-7": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend-8": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "friend-9": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "random": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "visualization": {
                        currentExercise: 0,
                        status: 0,
                    },
                }
            },
            "combo+": {
                sublevels: {
                    "combo+6": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "combo+7": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "combo+8": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "combo+9": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "random": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "visualization": {
                        currentExercise: 0,
                        status: 0,
                    },
                }
            },
            "combo-": {
                sublevels: {
                    "combo-6": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "combo-7": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "combo-8": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "combo-9": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "random": {
                        currentExercise: 0,
                        status: 0,
                    },
                    'visualization': {
                        currentExercise: 0,
                        status: 0,
                    },
                },
            },
            "free": {
                sublevels: {
                    "random": {
                        currentExercise: 0,
                        status: 0,
                    },
                    "visualization": {
                        currentExercise: 0,
                        status: 0,
                    },
                }
            },
        }
    }
    saveStatus = () => {
        let params = queryString.parse(this.props.location.search);
        if (Object.entries(params).length === 0) {
            console.log("no token: not saved")
        } else {
            const servers = {
                prod: "https://api.heygo.school",
                dev: "https://ih1608160.vds.myihor.ru"
            }
            let server = servers[params.source] || servers.prod;
            let saving = {
                progress: 0,
                startLevel: 1,
                record: 0,
                score: 0,
                gameState: this.state.gameState,
            }
            axios.put(`${server}/api/v0.1/courses-management/games`,
                { properties: JSON.stringify(saving) },
                { params: { token: params.token } }
            )
                .then(r => console.log(r.status))
                .catch(e => console.log(e));
        }
    }
    componentDidMount() {
        const servers = {
            prod: "https://api.heygo.school",
            dev: "https://ih1608160.vds.myihor.ru"
        }
        let params = queryString.parse(this.props.location.search);
        let server = servers[params.source] || servers.prod
        let newState = Object.assign({}, this.state);
        if (Object.entries(params).length === 0) {
            newState.loading = false;
            this.setState(newState);
        } else {
            axios.get(`${server}/api/v0.1/courses-management/games?token=${params.token}`)
                .then(response => {
                    newState.loading = false;
                    //let config = JSON.parse(response.data.data.config)
                    let properties = JSON.parse(response.data.data.properties);
                    if (Object.entries(properties).length === 0) {
                        console.log("Welcome to new game!")
                    } else {
                        if (properties.gameState !== undefined) {
                            newState.gameState = properties.gameState;
                        }
                    }
                    this.setState(newState);
                });
        }
    }
    delayState = () => {
        this.mydelay = setTimeout(() => {
            let newState = Object.assign({}, this.state);
            if (newState.tutorialState.sequence[newState.tutorialState.currentExercise].answer !== newState.value) {
                newState.visibility = false;
            }
            this.setState(newState);
        }, 4000);
    }
    setVisible = (e) => {
        clearInterval(this.mydelay)
        let newState = Object.assign({}, this.state);
        newState.visibility = true;
        this.setState(newState);
    }
    dragStart = (e) => {
        if (this.state.enableAbacus) {
            try {
                if (e.target.id === "sp0" || e.target.id === "circle") {
                    this.startResetDrag = true;
                    if (e.type === "touchstart") {
                        this.resetX0 = e.touches[0].clientX
                    } else {
                        this.resetX0 = e.clientX
                    }
                }
                let newState = Object.assign({}, this.state);
                if (newState.sublevel === "visualization") {
                    this.setVisible();
                }
                newState.visibility = true;
                newState.beadHeight5 = document.querySelector("#uc0").getBoundingClientRect().height - 8;
                newState.beadHeight1 = (document.querySelector("#bc0").getBoundingClientRect().height - 8) / 2.5;
                newState.dragged.c = parseInt(e.target.id[0]);
                newState.dragged.r = parseInt(e.target.id[2]);
                newState.levelState = "started";
                let c = parseInt(e.target.id[0]);
                let r = parseInt(e.target.id[2]);
                if (e.type === "touchstart") {
                    newState.abacusState[c][r].initialY = e.touches[0].clientY - this.state.abacusState[c][r].yOffset;
                } else {
                    newState.abacusState[c][r].initialY = e.clientY - this.state.abacusState[c][r].yOffset;
                }
                if (e.target !== e.currentTarget) {
                    // newState.abacusState[c][r].active = true;
                    newState.active = true;
                }
                this.setState(newState);
            } catch (e) {
            }
        }
    }
    drag = (e) => {
        if (this.state.enableAbacus) {
            let c, r;
            try {
                //   if (this.state.abacusState[c][r].active === true) {
                if (this.state.active === true) {
                    c = this.state.dragged.c;
                    r = this.state.dragged.r;
                    let newState = Object.assign({}, this.state);
                    let uch5 = newState.beadHeight5;
                    let uch1 = newState.beadHeight1;
                    newState.visibility = true;
                    let currentY;
                    if (e.type === "touchmove") {
                        currentY = e.touches[0].clientY - this.state.abacusState[c][r].initialY;
                    } else {
                        currentY = e.clientY - this.state.abacusState[c][r].initialY;
                    }
                    if (!this.state.abacusState[c][r].beadState) {
                        if (r === 0) {
                            if (currentY >= 0) {
                                if (currentY < uch5 / 4) {
                                    newState.abacusState[c][r].currentY = currentY
                                    //this.setTranslate(newState.abacusState[c][r].currentY, dragItem);
                                } else {
                                    currentY = uch5 / 2
                                    //this.setTranslate(newState.abacusState[c][r].currentY, dragItem);
                                    newState.abacusState[c][r].yOffset = currentY;
                                    newState.abacusState[c][r].currentY = currentY;
                                    newState.abacusState[c][r].beadState = 1;
                                    // newState.abacusState[c][r].active = false;
                                    newState.active = false;
                                    this.setState(newState);
                                    return false;
                                }
                            }
                        }
                        if ([1, 2, 3, 4].includes(r)) {
                            if (currentY < 0) {
                                if (currentY > -1 * uch1 / 4) {
                                    // this.setTranslate(currentY, dragItem);
                                    newState.abacusState[c][r].currentY = currentY;
                                    let firstOne = 1;
                                    for (let ind = 1; ind <= 4; ind++) {
                                        if (newState.abacusState[c][ind].beadState === 1) {
                                            firstOne = ind + 1;
                                        }
                                    }
                                    for (let ind = firstOne; ind <= r; ind++) {
                                        newState.abacusState[c][ind].currentY = currentY;
                                        newState.abacusState[c][ind].yOffset = currentY;
                                    }
                                } else {
                                    currentY = -1 * uch1 / 2;
                                    for (let ind = 1; ind <= r; ind++) {
                                        // let item = document.querySelector("#bead_" + c + "_" + ind);
                                        //this.setTranslate(newState.abacusState[c][r].currentY, item);
                                        newState.abacusState[c][ind].beadState = 1;
                                        newState.abacusState[c][ind].yOffset = currentY;
                                        newState.abacusState[c][ind].currentY = currentY;
                                    }
                                    // newState.abacusState[c][r].active = false;
                                    newState.active = false;
                                    this.setState(newState);
                                    return false;
                                }
                            }
                        }
                    }
                    if (newState.abacusState[c][r].beadState) {
                        if (r === 0) {
                            if (newState.abacusState[c][r].currentY > uch5 / 4 && newState.abacusState[c][r].currentY <= uch5 / 2) {
                                // this.setTranslate(newState.abacusState[c][r].currentY, dragItem);
                                newState.abacusState[c][r].currentY = currentY;
                                newState.abacusState[c][r].yOffset = currentY;
                            }
                            else if (currentY <= uch5 / 4) {
                                currentY = 0;
                                // this.setTranslate(newState.abacusState[c][r].currentY, dragItem);
                                newState.abacusState[c][r].currentY = currentY;
                                newState.abacusState[c][r].yOffset = currentY;
                                newState.abacusState[c][r].beadState = 0;
                                // newState.abacusState[c][r].active = false;
                                newState.active = false;
                                this.setState(newState);
                                return false;
                            }
                        }
                        if ([1, 2, 3, 4].includes(r)) {
                            if (currentY < 0) {
                                if (currentY <= -1 * uch1 / 4 && currentY > -1 * uch1 / 2) {
                                    let firstZero = 4;
                                    for (let ind = 4; ind > 0; ind--) {
                                        if (newState.abacusState[c][ind].beadState === 0) {
                                            firstZero = ind - 1;
                                        }
                                    }
                                    for (let ind = r; ind <= firstZero; ind++) {
                                        newState.abacusState[c][ind].currentY = currentY;
                                        newState.abacusState[c][ind].yOffset = currentY;
                                    }
                                }
                                if (currentY > -1 * uch1 / 4) {
                                    currentY = 0;
                                    //this.setTranslate(currentY, dragItem);
                                    for (let ind = r; ind <= 4; ind++) {
                                        newState.abacusState[c][ind].beadState = 0;
                                        newState.abacusState[c][ind].yOffset = currentY;
                                        newState.abacusState[c][ind].currentY = currentY;
                                    }
                                    // newState.abacusState[c][r].active = false;
                                    newState.active = false;
                                    this.setState(newState);
                                    return false;
                                }
                            }
                        }
                    }
                    this.setState(newState);
                }
            } catch (e) {
            }
        }
    }
    dragEnd = (e) => {
        if (this.state.enableAbacus) {
            if (this.startResetDrag) {
                this.startResetDrag = false;
                if (e.type === "touchstart") {
                    this.resetX1 = e.touches[0].clientX
                } else {
                    this.resetX1 = e.clientX
                }
                if (this.resetX1 < this.resetX0) {
                    this.resetAbacus();
                }
            }
            try {
                let c = this.state.dragged.c;
                let r = this.state.dragged.r;
                let newState = Object.assign({}, this.state);
                let uch5 = newState.beadHeight5;
                let uch1 = newState.beadHeight1;
                let currentY = newState.abacusState[c][r].currentY;
                if (!newState.abacusState[c][r].beadState) {
                    if (r === 0) {
                        if (currentY < uch5 / 2) {
                            currentY = 0;
                            newState.abacusState[c][r].yOffset = currentY;
                            newState.abacusState[c][r].currentY = currentY;
                        }
                    }
                    if ([1, 2, 3, 4].includes(r)) {
                        if (currentY > -1 * uch1 / 4) {
                            let firstOne = 1;
                            for (let ind = 1; ind <= 4; ind++) {
                                if (newState.abacusState[c][ind].beadState === 1) {
                                    firstOne = ind + 1;
                                }
                            }
                            for (let ind = firstOne; ind <= r; ind++) {
                                newState.abacusState[c][ind].currentY = 0;
                                newState.abacusState[c][ind].yOffset = 0;
                            }
                        }
                    }
                }
                if (newState.abacusState[c][r].beadState) {
                    if (r === 0) {
                        if (currentY > uch5 / 4) {
                            currentY = uch5 / 2;
                            newState.abacusState[c][r].currentY = currentY;
                            newState.abacusState[c][r].yOffset = currentY;
                        }
                    }
                    if ([1, 2, 3, 4].includes(r)) {
                        if (newState.abacusState[c][r].currentY > -1 * uch1 / 2) {
                            let firstZero = 4;
                            for (let ind = 4; ind > 0; ind--) {
                                if (newState.abacusState[c][ind].beadState === 0) {
                                    firstZero = ind - 1;
                                }
                            }
                            for (let ind = r; ind <= firstZero; ind++) {
                                currentY = -1 * uch1 / 2;
                                newState.abacusState[c][ind].yOffset = currentY;
                                newState.abacusState[c][ind].currentY = currentY;
                            }
                        }
                    }
                }
                newState.abacusState[c][r].initialY = currentY;
                newState.active = false;
                newState.dragged.c = null;
                newState.dragged.r = null;
                newState.value = calculateValue(newState.abacusState);
                try {
                    if (newState.level === "counting") {
                        let abacusSize;
                        if (newState.value === newState.tutorialState.sequence[newState.tutorialState.currentExercise]) {
                            if (newState.tutorialState.currentExercise < newState.tutorialState.sequence.length - 1) {
                                newState.tutorialState.currentExercise += 1;
                                newState.gameState[newState.level].sublevels[newState.sublevel].status = 1;
                                abacusSize = newState.tutorialState.sequence[newState.tutorialState.currentExercise];
                                newState.abacusState = resizeAbacus(abacusSize, newState.abacusState);
                            } else {
                                newState.enableAbacus = false;
                                newState.gameCompeted = true;
                                newState.gameState[newState.level].sublevels[newState.sublevel].status = 2;
                                //newState.tutorialState.currentExercise += 1;
                                //newState.tutorialState.currentExercise = 0;
                            }
                            if (newState.tutorialState.currentExercise % 10 === 0) {
                                this.saveStatus();
                            }
                        }
                    } else if (newState.sublevel === "visualization") {
                        if (newState.tutorialState.sequence[newState.tutorialState.currentExercise].answer === newState.value) {
                            if (newState.tutorialState.currentExercise === newState.tutorialState.sequence.length - 1) {
                                newState.gameCompeted = true;
                                newState.gameState[newState.level].sublevels[newState.sublevel].status = 2;
                                newState.tutorialState.currentExercise = 0;
                                clearInterval(this.mydelay);
                                if (newState.tutorialState.currentExercise === 10) {
                                    this.saveStatus();
                                }
                            }
                        }
                    } else {
                        if (newState.tutorialState.currentExercise <= newState.tutorialState.sequence.length - 1) {
                            let progressSum = 0;
                            for (let i = 0; i < newState.ansPosition + 1; i++) {
                                progressSum += newState.tutorialState.sequence[newState.tutorialState.currentExercise].seq[i];
                            }
                            if (newState.value === progressSum) {
                                newState.ansPosition += 1;
                            }
                            if (newState.tutorialState.currentExercise + 1 === newState.tutorialState.sequence.length) {
                                if (newState.tutorialState.sequence[newState.tutorialState.currentExercise].seq.length === newState.ansPosition &&
                                    newState.tutorialState.sequence[newState.tutorialState.currentExercise].answer === newState.value) {
                                    newState.gameCompeted = true;
                                    newState.gameState[newState.level].sublevels[newState.sublevel].status = 2;
                                    //newState.tutorialState.currentExercise = 0;
                                    if (newState.tutorialState.currentExercise === 10) {
                                        this.saveStatus();
                                    }
                                }
                            }
                        }
                    }
                } catch (e) {
                    
                }
                this.setState(newState);
                if (this.state.sublevel === "visualization") {
                    this.delayState();
                }
            } catch (e) {
            }
        }
    }
    resetAbacus = () => {
        let newState = Object.assign({}, this.state);
        if (newState.inFullAbacus) {
            newState.abacusState = generateEmptyAbacus(9);
        }
        else if (newState.level === "counting") {
            newState.abacusState = generateEmptyAbacus(String(newState.tutorialState.sequence[newState.tutorialState.currentExercise]).length);
        } else if (newState.sublevel === "visualization") {
            if (newState.value === newState.tutorialState.sequence[newState.tutorialState.currentExercise].answer) {
                newState.tutorialState.currentExercise += 1;
                newState.gameState[newState.level].sublevels[newState.sublevel].status = 1;
                if (newState.tutorialState.currentExercise === 10) {
                    this.saveStatus();
                }
                clearInterval(this.mydelay);
                this.delayState();
            }
            let Kolcifr = newState.tutorialState.sequence[newState.tutorialState.currentExercise].Kolcifr;
            newState.abacusState = generateEmptyAbacus(Kolcifr);
        } else {
            if (newState.value === newState.tutorialState.sequence[newState.tutorialState.currentExercise].answer) {
                newState.tutorialState.currentExercise += 1;
                newState.gameState[newState.level].sublevels[newState.sublevel].status = 1;
                if (newState.tutorialState.currentExercise === 10) {
                    this.saveStatus();
                }
            }
            let Kolcifr = this.getSuitableSize(newState.tutorialState.sequence[newState.tutorialState.currentExercise].answer, newState.tutorialState.sequence[newState.tutorialState.currentExercise].seq)
            newState.abacusState = generateEmptyAbacus(Kolcifr);
        }
        newState.ansPosition = 0;
        newState.value = 0;
        this.setState(newState);
    }
    resetStart = () => {
        document.getElementById('reset').style.backgroundPositionY = '20px';
    }
    resetEnd = () => {
        document.getElementById('reset').style.backgroundPositionY = '10px';
        this.resetAbacus();
    }
    restartTutorial = () => {
        let newState = Object.assign({}, this.state);
        newState.value = 0;
        newState.ansPosition = 0;
        newState.enableAbacus = true;
        newState.visibility = true;
        clearInterval(this.mydelay);
        let sec = generateSecuence(this.state.level, this.state.sublevel);
        newState.tutorialState.sequence = sec;
        newState.tutorialState.currentExercise = 0;
        if (newState.level === "counting") {
            let abacusSize = newState.tutorialState.sequence[newState.tutorialState.currentExercise];
            newState.abacusState = generateEmptyAbacus(String(abacusSize).length);
        } else {
            if (newState.sublevel === "visualization") {
                this.delayState();
            }
            let Kolcifr = this.getSuitableSize(newState.tutorialState.sequence[newState.tutorialState.currentExercise].answer, newState.tutorialState.sequence[newState.tutorialState.currentExercise].seq)
            newState.abacusState = generateEmptyAbacus(Kolcifr);
        }
        newState.gameCompeted = false;
        newState.gameState[newState.level].sublevels[newState.sublevel].status = 0;
        this.setState(newState);
        this.saveStatus();
    }
    startGame = (level, sublevel) => {
        let newState = Object.assign({}, this.state);
        newState.gameStarted = true;
        newState.gameCompeted = false;
        newState.enableAbacus = true;
        newState.ansPosition = 0;
        newState.value = 0;
        newState.inMenu = false;
        newState.level = level;
        newState.sublevel = sublevel;

        let sec = generateSecuence(level, sublevel);
        newState.tutorialState = {
            currentExercise: this.state.gameState[level].sublevels[sublevel].currentExercise,
            sequence: sec,
        }
        if (newState.level === "counting") {
            let abacusSize = newState.tutorialState.sequence[newState.tutorialState.currentExercise];
            newState.abacusState = generateEmptyAbacus(String(abacusSize).length);
            if (this.state.gameState[level].sublevels[sublevel].status === 2) {
                // newState.tutorialState.currentExercise = 0;
                newState.gameCompeted = true;
                newState.abacusState = generateEmptyAbacus(2);
                newState.value = 0;
                newState.ansPosition = 0;
            }
        } else {
            let maxKolcifr = this.getSuitableSize(newState.tutorialState.sequence[newState.tutorialState.currentExercise].answer, newState.tutorialState.sequence[newState.tutorialState.currentExercise].seq)
            newState.abacusState = generateEmptyAbacus(maxKolcifr);
            if (this.state.gameState[level].sublevels[sublevel].status === 2) {
                newState.gameCompeted = true;
                newState.value = 0;
                newState.ansPosition = 0;
            }
            if (newState.sublevel === "visualization") {
                this.delayState();
            }
        }
        this.setState(newState);
    }
    numDigits = (x) => {
        return Math.max(Math.floor(Math.log10(Math.abs(x))), 0) + 1;
    }
    getSuitableSize = (x, y) => {
        let ny = [...y]
        ny.push(x);
        let f = ny.map(this.numDigits);
        return Math.max(...f)
    }
    showMenu = () => {
        let newState = Object.assign({}, this.state);
        newState.inMenu = true;
        newState.inTitle = false;
        newState.inFullAbacus = false;
        try {
            newState.gameState[this.state.level].sublevels[this.state.sublevel].currentExercise = this.state.tutorialState.currentExercise;
            this.saveStatus();
        }
        catch {
        }
        clearInterval(this.mydelay)
        newState.visibility = true;
        this.setState(newState);
    }
    changeLevel = (level) => {
        let newState = Object.assign({}, this.state);
        newState.level = level;
        this.setState(newState);
    }
    showFullAbacus = () => {
        let newState = Object.assign({}, this.state);
        newState.inMenu = false;
        newState.inFullAbacus = true;
        newState.enableAbacus = true;
        newState.abacusState = generateEmptyAbacus(9);
        newState.value = 0;
        this.setState(newState);
    }
    componentDidUpdate() {
        let intermedateSum = 0;
        for (let i = 0; i <= this.state.ansPosition; i++) {
            intermedateSum += this.state.seq[i];
        }
        if (this.state.value === intermedateSum) {
            let newState = Object.assign({}, this.state);
            newState.ansPosition = this.state.ansPosition + 1;
            if (newState.ansPosition === 3) {
                newState.levelState = "finished";
            }
            this.setState(newState);
        }
    }
    render() {
        return (
            <AbacusContext.Provider
                value={{
                    state: this.state,
                    actions: {
                        restartTutorial: this.restartTutorial,
                        resetStart: this.resetStart,
                        resetEnd: this.resetEnd,
                        dragStart: this.dragStart,
                        drag: this.drag,
                        dragEnd: this.dragEnd,
                        startTimer: this.startTimer,
                        stopTimer: this.stopTimer,
                        showMenu: this.showMenu,
                        changeLevel: this.changeLevel,
                        startGame: this.startGame,
                        showFullAbacus: this.showFullAbacus,
                    },
                }}
            >
                {this.props.children}
            </AbacusContext.Provider>
        );
    }
}
export const AbacusConsumer = AbacusContext.Consumer
/*
баги:
4) ссылки на урвни
5) сохранение результав и загрузка
6
*/