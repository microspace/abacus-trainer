import utils from '../../libs/generator'

function generateEx(cfg, mode, vmode) {
    let flag = 0;
    let res, ans;
    while (flag === 0) {
        res = utils.generate(cfg);
        ans = res.answer.reverse().reduce(function (a, b) { return a * 10 + b });
        ans === 0 ? flag = 0 : flag = 1;
    }
    return { seq: res.seq, answer: ans, Kolcifr: cfg.Kolcifr, mode: mode, vmode: vmode }
}

const generateEmptyAbacus = (ans) => {
    let c = ans;
    let r = 5;
    let abacus = [];
    for (let j = 1; j <= c; j++) {
        abacus.push([])
        for (let i = 1; i <= r; i++) {
            abacus[j - 1].push({
                beadState: 0,
                active: false,
                currentY: 0,
                initialY: 0,
                yOffset: 0,
            })
        }
    }
    return abacus
}

const resizeAbacus = (ans, oldAbacus) => {
    //document.getElementById("reset").setAttribute("class", "reset");
    let c = String(ans).length;
    let r = 5;
    let emptycolumn = [];

    for (let i = 1; i <= r; i++) {
        emptycolumn.push({
            beadState: 0,
            active: false,
            currentY: 0,
            initialY: 0,
            yOffset: 0,
        })

    }
    const addedrowcount = c - oldAbacus.length;
    for (let i = 1; i <= addedrowcount; i++) {
        oldAbacus.push(emptycolumn);
    }
    return oldAbacus
}

function calculateValue(abacusState) {

    let sum = 0;
    for (let i = 0; i < abacusState.length; i++) {
        sum += (abacusState[i][0].beadState * 5 + Math.max(abacusState[i][1].beadState, abacusState[i][2]
            .beadState * 2, abacusState[i][3].beadState * 3, abacusState[i][4].beadState * 4)) * (10 ** i);
    }
    return sum;
}
const checked_cifr = {
    "pm1234": [1, 2, 3, 4],
    "pm5": [5],
    "pm6": [6],
    "pm7": [7],
    "pm8": [8],
    "pm9": [9],
    "random": [1, 2, 3, 4, 5, 6, 7, 8, 9],
    "visualization": [1, 2, 3, 4, 5, 6, 7, 8, 9],
    "brother+1": [1],
    "brother+2": [2],
    "brother+3": [3],
    "brother+4": [4],
    "brother+1234": [1, 2, 3, 4],
    "brother-1": [1],
    "brother-2": [2],
    "brother-3": [3],
    "brother-4": [4],
    "brother-1234": [1, 2, 3, 4],
    "friend+1": [1],
    "friend+2": [2],
    "friend+3": [3],
    "friend+4": [4],
    "friend+5": [5],
    "friend+6": [6],
    "friend+7": [7],
    "friend+8": [8],
    "friend+9": [9],
    "friend+1,2,3,4,5,6,7,8,9": [1, 2, 3, 4, 5, 6, 7, 8, 9],
    "friend-1": [1],
    "friend-2": [2],
    "friend-3": [3],
    "friend-4": [4],
    "friend-5": [5],
    "friend-6": [6],
    "friend-7": [7],
    "friend-8": [8],
    "friend-9": [9],
    "friend-1,2,3,4,5,6,7,8,9": [1, 2, 3, 4, 5, 6, 7, 8, 9],
    "combo+6": [6],
    "combo+7": [7],
    "combo+8": [8],
    "combo+9": [9],
    "combo+6,7,8,9": [6, 7, 8, 9],
    "combo-6": [6],
    "combo-7": [7],
    "combo-8": [8],
    "combo-9": [9],
    "combo-6,7,8,9": [6, 7, 8, 9],
    "free": [1, 2, 3, 4, 5, 6, 7, 8, 9],
}

const generateGenerate = (sublevel, levelindex, operation, Kolslog, Kolcifr, exercises) => {
    let arr1 =[];
    for (let i = 0; i < exercises; i++) {
        const cfg = {
            Checked_cifr: checked_cifr[sublevel],
            Level: levelindex,
            Operation: operation,
            Kolslog: Kolslog,
            Kolcifr: Kolcifr
        }
        arr1.push(generateEx(cfg, sublevel, sublevel))
    }
    return arr1;
}
const generateSecuence = (level, sublevel) => {

    if (level === "counting") {
        const exercises = 20;
        if (sublevel === "forward") {
            return [...Array(100).keys()].slice(1)
        } else if (sublevel === "reverse") {
            return [...Array(100).keys()].slice(1).reverse();
        } else if (sublevel === "random") {
            const digits = 2;
            return getRandomInts(exercises, 10 ** (digits - 1) - 1, 10 ** digits);
        }
    } else if (level === "plusminus") {
        return generateGenerate(sublevel, 1, 2, 3, 2, 20);
    } else if (level === "brother+") {
        return generateGenerate(sublevel, 2, 0, 3, 2, 20);
    } else if (level === "brother-") {
        return generateGenerate(sublevel, 2, 1, 3, 2, 20);
    } else if (level === "friend+") {
        return generateGenerate(sublevel, 3, 0, 3, 2, 20);
    } else if (level === "friend-") {
        return generateGenerate(sublevel, 3, 1, 3, 2, 20);
    } else if (level === "combo+") {
        return generateGenerate(sublevel, 4, 0, 3, 2, 20);
    } else if (level === "combo-") {
        return generateGenerate(sublevel, 4, 1, 3, 2, 20);
    } else if (level === "free") {
        return generateGenerate(sublevel, 5, 2, 3, 2, 20);
    } 
}




function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function getRandomInts(num, min, max) {
    var ints = [];
    while (ints.length < num) {
        var randNum = getRandomInt(min, max);
        if (ints.indexOf(randNum) === -1) {
            ints.push(randNum);
        }
    }

    while (ints[0] === 0) {
        shuffleArray(ints)
    }
    return ints;
}

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}


export {
    generateEmptyAbacus,
    calculateValue,
    resizeAbacus,
    generateEx,
    generateSecuence
}
